<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnAssureurId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('patients', function (Blueprint $table) {
             $table->integer('assureur_id')->unsigned()->after('id');
            $table->foreign('assureur_id')->references('id')->on('assureurs');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('patients', function (Blueprint $table) {
        $table->dropForeign(['assureur_id']);
        $table->dropColumn('assureur_id');
        });
    }
}
