<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnHospitalId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('navettes', function (Blueprint $table) {
            $table->integer('hospital_id')->unsigned()->after('id');
            $table->foreign('hospital_id')->references('id')->on('hospitals');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('navettes', function (Blueprint $table) {
        $table->dropForeign(['hospital_id']);
        $table->dropColumn('hospital_id');

        });
    }
}
