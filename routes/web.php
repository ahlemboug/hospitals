<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('hospitals','HospitalController');
Route::resource('medicas','MedicaController');
Route::resource('assureurs','AssureurController');
Route::resource('actes','ActeController');
Route::resource('patients','PatientController');
Route::get('navettes','NavetteController@create');

// soft delete

Route::any('/hospitals/delete/{id}', 'HospitalController@delete');
Route::any('/medicas/delete/{id}', 'MedicaController@delete');
Route::any('/actes/delete/{id}', 'ActeController@delete');
Route::any('/assureurs/delete/{id}', 'AssureurController@delete');
Route::any('/patients/delete/{id}', 'PatientController@delete');

Route::post('/navettes/create/gethospital','NavetteController@getHospital');
Route::get('/navettes/create/gethospital','NavetteController@getHospital');


Route::post('/navettes/create/getpatient','NavetteController@getPatient');
Route::get('/navettes/create/getpatient','NavetteController@getPatient');



