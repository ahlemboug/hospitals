@extends('layouts.master')

@section('content')

	<div class="container"> 
		<div class="row">
			<div class="col-md-12">
			<h1>Ajoute medicas</h1>
			<form method="post" action="{{url('medicas/'.$medica->id) }}">
			<input type="hidden" name="_method" value="PUT">
			{{ csrf_field() }}
			<label> Nom Medica</label>
			<input type="text" name="name" value="{{ $medica->name }}">
			<br>
			<label>Code</label>
			<input type="text" name="code_medica" value="{{ $medica->code_medica }}">
			<br>
			<label>Prix</label>
			<input type="text" name="prix" value="{{ $medica->prix }}"><br>
			
			<button type="submit" class="btn btn-primary">
                Enregister
            </button>
            </form>
			</div>
		</div>
	</div>
@endsection