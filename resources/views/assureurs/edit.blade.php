@extends('layouts.master')

@section('content')

	<div class="container"> 
		<div class="row">
			<div class="col-md-12">
			<h1>Ajoute assureurs</h1>
			<form method="post" action="{{url('assureurs/'.$assureur->id) }}">
			<input type="hidden" name="_method" value="PUT">
			{{ csrf_field() }}
			<label>Nom Assureur</label>
			<input type="text" name="name" value="{{ $assureur->name }}">
			<br>
			<label>Code</label>
			<input type="text" name="code_assureur" value="{{ $assureur->code_assureur }}">
			<br>
		
			<button type="submit" class="btn btn-primary">
                Enregister
            </button>
            </form>
			</div>
		</div>
	</div>
@endsection