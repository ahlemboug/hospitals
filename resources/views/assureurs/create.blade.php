@extends('layouts.master')
@section('content')

	<div class="container"> 
		<div class="row">
			<div class="col-md-12">

			@if(session()->has('success'))

			<div class="alert alert-success">

				{{ session()->get('success')}}
			</div> 
			@endif

			<h1>Ajoute assureurs</h1>
			<form method="post" action="/assureurs">
			{{ csrf_field() }}
			
			<label>Nom Assureur</label>
			<input type="text" name="name">
			<br>
			<label>Code</label>
			<input type="text" name="code_assureur">
			<br>
			
			<button type="submit" class="btn btn-primary">
                Ajouter
            </button>
            </form>
			</div>
		</div>
	</div>
@endsection