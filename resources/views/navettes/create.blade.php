@extends('layouts.master')
@section('content')
	<style>
		h1{  
    		font-size: 100px; /* Titres de 40 pixels */
			}
			.container{

			   background-image: url(/images/malade02.jpg);
			   background-repeat:no-repeat;
               }
       </style>

	<div class="container"> 
		<div class="row">
			<div class="col-md-12">

			@if(session()->has('success'))

			<div class="alert alert-success">

				{{ session()->get('success')}}
			</div> 
			@endif

			<h1>Fiche navette</h1>

			<form id="form" method="post" action="/">
			{{ csrf_field() }}

			

			<label>Num Patient</label>
			<input type="text" id="num_patient" name="num_patient">
			<i class="fa fa-refresh" id="load"></i>
			<br>

			<label>Nom Patient</label>
			<input type="text" name="name">
			<br>

			<label>Prenom Patient</label>
			<input type="text" name="last_name">
			<br>

			<label>Adresse Patient</label>
			<input type="text" name="address">
			<br>

			<label>Medicament</label>
			
			<select name="name_medica">
       			<option>Sélectionner un médicament</option>
        		<option value="acheter">Acheter (100 or)</option>
        		<option value="vendre">Vendre (90 or)</option>
    		</select>

			<label>Quantité</label>
			<input type="text" name="qte">
			
			<br>

			
			<button type="submit" class="btn btn-primary">
                Enregistrer
            </button>
            </form>
			</div>
		</div>
	</div>

	<script>
				$(document).ready(function(){
			$('#num_patient').keydown();
			$('#num_patient').keyup(function(){
				console.log($('#num_patient').val());
				$('#load').addClass("fa-spin");
				$.post("/navettes/create/getpatient",
				{
					"_token":$('#form').find('input[name=_token]').val(),
					"id":$('#num_patient').val(),
				},function(data,status){
					console.log("status : " + status+"\n"+
								"name : "+data.name);
					$('#form').find('input[name=name]').val(data.name);
					$('#form').find('input[name=last_name]').val(data.last_name);
					$('#form').find('input[name=address]').val(data.address);
					$('#load').removeClass("fa-spin");
				});
			});
		});
	</script>
@endsection