@extends('layouts.master')

@section('content')

	<div class="container"> 
		<div class="row">
			<div class="col-md-12">
			<h1>Ajoute actes</h1>
			<form method="post" action="{{url('actes/'.$acte->id) }}">
			<input type="hidden" name="_method" value="PUT">
			{{ csrf_field() }}
			<label>Nom Acte</label>
			<input type="text" name="name" value="{{ $acte->name }}">
			<br>
			<label>Code</label>
			<input type="text" name="code_acte" value="{{ $acte->code_acte }}">
			<br>
			<label>Prix</label>
			<input type="text" name="prix" value="{{ $acte->prix }}"><br>
			<button type="submit" class="btn btn-primary">
                Enregister
            </button>
            </form>
			</div>
		</div>
	</div>
@endsection