@extends('layouts.master')


@section('content')
	<style>
		h1{  
    		font-size: 100px; /* Titres de 40 pixels */
			}


       </style>

	<div class="container"> 
		<div class="row">
			<div class="col-md-12">

			@if(session()->has('success'))

			<div class="alert alert-success">

				{{ session()->get('success')}}
			</div> 
			@endif
			<h1>La listes des actes</h1>
			<form action="{{ route('actes.index')}}" method="get" class="form-inline">
				<div class="form-group">
					<input type="text" class="form-control" name="s" placeholder="keyword" value="{{isset($s) ? $s : '' }}">
				</div>
				<div class="form-group">
					 <button class="btn btn-success" type="submit">Search</button>
				</div>
			</form>
				<div class="pull-right">
					<a href="{{url('actes/create')}}" class="btn btn-success">Nouveau acte</a>
				</div>
				
				
					<table class="table">
						<head>
							<tr><th>id</th>
							    <th>Code</th>
								<th>Nom</th>
								<th>Prix</th>
								<th>Date</th>
								<th>Actions</th>

							</tr>
						</head>

						<body>
						  @foreach($actes as $acte)
							<tr>
								<td>{{$acte->id}}</td>
								<td>{{$acte->code_acte}}</td>
								<td>{{$acte->name}}</td>
								<td>{{$acte->prix}}</td>
								<td>{{$acte->created_at}}</td>

								<td>
									
									<form action="{{url('actes/'.$acte->id)}}" method="get">

									 {{ csrf_field() }}
									 {{ method_field('DELETE') }} 

									<a href="" class="btn btn-primary">Detail</a>
									<a href="{{url('actes/'.$acte->id.'/edit')}}" class="btn btn-default">Editer</a>
									<a href="/actes/delete/{{ $acte->id }}" class="btn btn-danger">Supprimer</a>
									</form>
								</td>
							</tr>
						  @endforeach

						</body>
					</table>

					{{$actes->appends(['s' => $s])->links()}}
			</div>
		</div>
	</div>
@endsection