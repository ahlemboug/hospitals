@extends('layouts.master')
@section('content')

	<div class="container"> 
		<div class="row">
			<div class="col-md-12">

			@if(session()->has('success'))

			<div class="alert alert-success">

				{{ session()->get('success')}}
			</div> 
			@endif

			<h1>Ajoute hospitals</h1>
			<form method="post" action="/hospitals">
			{{ csrf_field() }}
			
			<label>Nom Hopital</label>
			<input type="text" name="name_hos">
			<br>
			<label>Address</label>
			<input type="text" name="address"><br>
			<label>Code</label>
			<input type="text" name="code_hospital"><br>
			<label>Mot de pass</label>
			<input type="text" name="mdp_hospital"><br>
			<button type="submit" class="btn btn-primary">
                Ajouter
            </button>
            </form>
			</div>
		</div>
	</div>
@endsection