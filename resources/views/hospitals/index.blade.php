@extends('layouts.master')


@section('content')
	 <style>
		h1{  
    		font-size: 100px; /* Titres de 40 pixels */
			}

       </style>

	<div class="container"> 
		<div class="row">
			<div class="col-md-12">

			@if(session()->has('success'))

			<div class="alert alert-success">

				{{ session()->get('success')}}
			</div> 
			@endif
			<h1>La listes des hopiteaux</h1>
			
			<form action="{{ route('hospitals.index')}}" method="get" class="form-inline">
				<div class="form-group">
					<input type="text" class="form-control" name="s" placeholder="keyword" value="{{isset($s) ? $s : '' }}">
				</div>

				<div class="form-group">
					 <button class="btn btn-success" type="submit">Search</button>
				</div>
			</form>
				<div class="pull-right">
					<a href="{{url('hospitals/create')}}" class="btn btn-success">Nouveau hospital</a>
				</div>
				
				
					<table class="table">
						<head>
							<tr>
								<th>id</th>
								<th>Code</th>
								<th>Mot de pass</th>
								<th>Nom</th>
								<th>Adresse</th>
								<th>Date</th>
								<th>Actions</th>

							</tr>
						</head>

						<body>
						  @foreach($hospitals as $hospital)
							<tr>
								
								<td>{{$hospital->id}}</td>
								<td>{{$hospital->code_hospital}}</td>
								<td>{{$hospital->mdp_hospital}}</td>
								<td>{{$hospital->name_hos}}</td>
								<td>{{$hospital->address}}</td>
								<td>{{$hospital->created_at}}</td>

								<td>
									
									<form action="{{url('hospitals/'.$hospital->id)}}" method="get">

									 {{ csrf_field() }}
									 {{ method_field('DELETE') }} 

									<a href="" class="btn btn-primary">Detail</a>
									<a href="{{url('hospitals/'.$hospital->id.'/edit')}}" class="btn btn-default">Editer</a>
									<a href="/hospitals/delete/{{ $hospital->id }}" class="btn btn-danger">Supprimer</a>

									</form>
								</td>
							</tr>
						  @endforeach

						</body>
					</table>
					{{$hospitals->appends(['s' => $s])->links()}}

			</div>
		</div>
	</div>
@endsection