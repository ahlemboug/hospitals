@extends('layouts.master')

@section('content')

	<div class="container"> 
		<div class="row">
			<div class="col-md-12">
			<h1>Modifier hospitals</h1>

			<form method="post" action="{{url('hospitals/'.$hospital->id) }}">
			<input type="hidden" name="_method" value="PUT">
			{{ csrf_field() }}
			<label>Hopital</label>
			<input type="text" name="name" value="{{ $hospital->name_hos }}">
			<br>
			<label>Address</label>
			<input type="text" name="address" value="{{ $hospital->address }}"><br>
			<label>Code</label>
			<input type="text" name="code_hospital" value="{{ $hospital->code_hospital }}"><br>
			<label>Mdp</label>
			<input type="text" name="mdp_hospital" value="{{ $hospital->mdp_hospital }}"><br>
			<button type="submit" class="btn btn-primary">
                Enregister
            </button>
            </form>
			</div>
		</div>
	</div>
@endsection