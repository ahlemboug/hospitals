@extends('layouts.master')
@section('content')

	<div class="container"> 
		<div class="row">
			<div class="col-md-12">

			@if(session()->has('success'))

			<div class="alert alert-success">

				{{ session()->get('success')}}
			</div> 
			@endif

			<h1>Ajoute Patient</h1>
			<form method="post" action="/patients">
			{{ csrf_field() }}
			
			<label>Nom Patient</label>
			<input type="text" name="last_name">
			<br>
			<label>Prenom patient</label>
			<input type="text" name="name"><br>
			<label>Adresse Patient</label>
			<input type="text" name="address"><br>
			<br>
			<button type="submit" class="btn btn-primary">
                Ajouter
            </button>
            
            </form>
			</div>
		</div>
	</div>
@endsection