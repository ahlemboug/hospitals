@extends('layouts.master')

@section('content')

	<div class="container"> 
		<div class="row">
			<div class="col-md-12">
			<h1>Modifier Patient</h1>
			
			<form method="post" action="{{url('patients/'.$patient->id) }}">
			<input type="hidden" name="_method" value="PUT">
			{{ csrf_field() }}

			<label>Nom</label>
			<input type="text" name="last_name" value="{{ $patient->last_name }}">
			<br>

			<label>Prenom</label>
			<input type="text" name="name" value="{{ $patient->name }}">
			<br>

			<label>address</label>
			<input type="text" name="address" value="{{ $patient->address }}"><br>
			<br>

			<button type="submit" class="btn btn-primary">
                Enregister
            </button>
            </form>
			</div>
		</div>
	</div>
@endsection