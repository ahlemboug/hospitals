<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Hospital extends Model
{

   public function scopeSearch($query, $s){
   	return $query->where('name_hos','like', $s.'%');
   }

}
