<?php

namespace App\Http\Controllers;

use App\Acte;
use Illuminate\Http\Request;
use Auth;

class ActeController extends Controller
{

 public function __construct(){

        $this->middleware('auth');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $s = $request->input('s');
        $actes = Acte::where('deleted','NOT',1)->latest()
        ->search($s)
        ->paginate(20);
        return view("actes.index",compact('actes','s'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('actes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'prix'=>'required',
            'code_acte'=>'required',
            ]);
        $acte = new acte();
        $acte->name = $request['name'];
        $acte->code_acte = $request['code_acte'];
        $acte->prix = $request['prix'];
        $acte->save();

        return redirect()->route('actes.index')
        ->with('flash_message','acte ajoute');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\acte  $acte
     * @return \Illuminate\Http\Response
     */
    public function show(Acte $acte)
    {
        return redirect()->route('actes.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\acte  $acte
     * @return \Illuminate\Http\Response
     */
    public function edit(Acte $acte)
    {
        return view('actes.edit',compact('acte'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\acte  $acte
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Acte $acte)
    {
        $this->validate($request,[
            'name'=>'required',
            'prix'=>'required',
            'code_acte'=>'required'
            ]);
        $acte->name = $request['name'];
        $acte->code_acte = $request['code_acte'];
        $acte->prix = $request['prix'];
        $acte->save();

        return redirect()->route('actes.index')
        ->with('flash_message','acte mais a jour');
   
    }
   

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\acte  $acte
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id){
        

        $acte = acte::find($id);
        $acte->deleted  = 1;
        dd($acte);
        $acte->save();
       
    


        return redirect()->route('actes.index')
        ->with('flash_message','acte supprimé');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\acte  $acte
     * @return \Illuminate\Http\Response
     */
    public function delete($id){
        

        $acte = Acte::find($id);
        $acte->deleted  = 1;
        $acte->save();
       
    


        return redirect()->route('actes.index')
        ->with('flash_message','acte supprimé');
    }
}
