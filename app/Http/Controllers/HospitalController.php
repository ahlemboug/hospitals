<?php

namespace App\Http\Controllers;

use App\Hospital;
use Illuminate\Http\Request;
use Auth;

class HospitalController extends Controller
{
    
 public function __construct(){

        $this->middleware('auth');

    }
    
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index(Request $request)
    {
        $s = $request->input('s');
        $hospitals = Hospital::where('deleted','NOT',1)->latest()
        ->search($s)
        ->paginate(20);
        return view("hospitals.index",compact('hospitals','s'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('hospitals.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name_hos'=>'required',
            'address'=>'required',
            'mdp_hospital'=>'required',
            'code_hospital'=>'required'
            ]);
        $hospital = new Hospital();
        $hospital->name = $request['name_hos'];
        $hospital->address = $request['address'];
        $hospital->mdp_hospital = $request['mdp_hospital'];
        $hospital->code_hospital = $request['code_hospital'];
        $hospital->save();

        return redirect()->route('hospitals.index')
        ->with('flash_message','hospital ajoute');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Hospital  $hospital
     * @return \Illuminate\Http\Response
     */
    public function show(Hospital $hospital)
    {
        return redirect()->route('hospitals.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Hospital  $hospital
     * @return \Illuminate\Http\Response
     */
    public function edit(Hospital $hospital)
    {
        return view('hospitals.edit',compact('hospital'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Hospital  $hospital
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Hospital $hospital)
    {
        $this->validate($request,[
            'name_hos'=>'required',
            'address'=>'required',
            'mdp_hospital'=>'required',
            'code_hospital'=>'required'
            ]);
        $hospital->name = $request['name_hos'];
        $hospital->address = $request['address'];
        $hospital->mdp_hospital= $request['mdp_hospital'];
        $hospital->code_hospital= $request['code_hospital'];
        $hospital->save();

        return redirect()->route('hospitals.index')
        ->with('flash_message','hospital mais a jour');
   
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Hospital  $hospital
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id){
        

        $hospital = Hospital::find($id);
        $hospital->deleted  = 1;
        dd($hospital);
        $hospital->save();
       
    


        return redirect()->route('hospitals.index')
        ->with('flash_message','hospital supprimé');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Hospital  $hospital
     * @return \Illuminate\Http\Response
     */
    public function delete($id){
        

        $hospital = Hospital::find($id);
        $hospital->deleted  = 1;
        $hospital->save();
       
    


        return redirect()->route('hospitals.index')
        ->with('flash_message','hospital supprimé');
    }

    function getHospital(){
        $code_hospital = Input::get('code_hospital');
        $hospital = Hospital::find($code_hospital);
        return response()->json($hospital);
    }

}
