<?php

namespace App\Http\Controllers;

use App\Patient;
use Illuminate\Http\Request;
use Auth;

class PatientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){

        $this->middleware('auth');

    }
    
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index(Request $request)
    {
        $s = $request->input('s');
        $patients = Patient::where('deleted','NOT',1)->latest()
        ->search($s)
        ->paginate(20);
        return view("patients.index",compact('patients','s'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('patients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'last_name'=>'required',
            'name'=>'required',
            'address'=>'required'
            ]);
        $patient = new Patient();
        $patient->last_name = $request['last_name'];
        $patient->name = $request['name'];
        $patient->address = $request['address'];
        
        $patient->save();

        return redirect()->route('patients.index')
        ->with('flash_message','patient ajoute');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function show(Patient $patient)
    {
        return redirect()->route('patients.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function edit(Patient $patient)
    {
        return view('patients.edit',compact('patient'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Patient $patient)
    {
        $this->validate($request,[
            'last_name'=>'required',
            'name'=>'required',
            'address'=>'required'
            ]);
        $patient->last_name = $request['last_name'];
        $patient->name = $request['name'];
        $patient->address = $request['address'];
        
        $patient->save();

        return redirect()->route('patients.index')
        ->with('flash_message','patient mais a jour');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        
        $patient = Patient::find($id);
        $patient->deleted  = 1;
        dd($patient);
        $patient->save();
       

        return redirect()->route('patients.index')
        ->with('flash_message','patient supprimé');
    }

        public function delete($id){
        

        $patient = Patient::find($id);
        $patient->deleted  = 1;
        $patient->save();
       
    
        return redirect()->route('patients.index')
        ->with('flash_message','patient supprimé');
    }
}
