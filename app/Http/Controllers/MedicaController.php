<?php

namespace App\Http\Controllers;

use App\Medica;
use Illuminate\Http\Request;

use Auth;

class MedicaController extends Controller
{

 public function __construct(){

        $this->middleware('auth');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   
    public function index(Request $request)
    {
        $s = $request->input('s');
        $medicas = Medica::where('deleted','NOT',1)->latest()
        ->search($s)
        ->paginate(20);
        return view("medicas.index",compact('medicas','s'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('medicas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'prix'=>'required',
            'code_medica'
            ]);
        $medica = new medica();
        $medica->name = $request['name'];
        $medica->code_medica = $request['code_medica'];
        $medica->prix = $request['prix'];
        $medica->save();

        return redirect()->route('medicas.index')
        ->with('flash_message','medica ajoute');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\medica  $medica
     * @return \Illuminate\Http\Response
     */
    public function show(Medica $medica)
    {
        return redirect()->route('medicas.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\medica  $medica
     * @return \Illuminate\Http\Response
     */
    public function edit(Medica $medica)
    {
        return view('medicas.edit',compact('medica'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\medica  $medica
     * @return \Illuminate\Http\Response
     */
   
    public function update(Request $request, Medica $medica)
    {
        $this->validate($request,[
            'name'=>'required',
            'prix'=>'required',
            'code_medica'=>'required'
            ]);
        $medica->name = $request['name'];
        $medica->prix = $request['prix'];
        $medica->code_medica = $request['code_medica'];
        $medica->save();

        return redirect()->route('medicas.index')
        ->with('flash_message','medica mais a jour');
   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\medica  $medica
     * @return \Illuminate\Http\Response
     */
   public function destroy(Request $request,$id){
        

        $medica = medica::find($id);
        $medica->deleted  = 1;
        dd($medica);
        $medica->save();
       
    


        return redirect()->route('medicas.index')
        ->with('flash_message','medica supprimé');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\medica  $medica
     * @return \Illuminate\Http\Response
     */
    public function delete($id){
        

        $medica = medica::find($id);
        $medica->deleted  = 1;
        $medica->save();
       
    


        return redirect()->route('medicas.index')
        ->with('flash_message','medica supprimé');
    }
}
