<?php

namespace App\Http\Controllers;

use App\Assureur;
use Illuminate\Http\Request;
use Auth;

class assureurController extends Controller
{

 public function __construct(){

        $this->middleware('auth');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $s = $request->input('s');
        $assureurs = Assureur::where('deleted','NOT',1)->latest()
        ->search($s)
        ->paginate(20);
        return view("assureurs.index",compact('assureurs','s'));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('assureurs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'code_assureur'=>'required'
            ]);
        $assureur = new Assureur();
        $assureur->name = $request['name'];
         $assureur->code_assureur = $request['code_assureur'];
        
        $assureur->save();

        return redirect()->route('assureurs.index')
        ->with('flash_message','assureur ajoute');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\assureur  $assureur
     * @return \Illuminate\Http\Response
     */
    public function show(Assureur $assureur)
    {
        return redirect()->route('assureurs.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\assureur  $assureur
     * @return \Illuminate\Http\Response
     */
    public function edit(Assureur $assureur)
    {
        return view('assureurs.edit',compact('assureur'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\assureur  $assureur
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, Assureur $assureur)
    {
        $this->validate($request,[
            'name'=>'required',
            'code_assureur'=>'required'
            
            ]);
        $assureur->name = $request['name'];
        $assureur->code_assureur = $request['code_assureur'];
        $assureur->save();

        return redirect()->route('assureurs.index')
        ->with('flash_message','assureur mais a jour');
   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\assureur  $assureur
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id){
        

        $assureur = Asssureur::find($id);
        $assureur->deleted  = 1;
        dd($assureur);
        $assureur->save();
       
    


        return redirect()->route('assureurs.index')
        ->with('flash_message','assureur supprimé');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\assureur  $assureur
     * @return \Illuminate\Http\Response
     */
    public function delete($id){
        

        $assureur = Assureur::find($id);
        $assureur->deleted  = 1;
        $assureur->save();
       
    


        return redirect()->route('assureurs.index')
        ->with('flash_message','assureur supprimé');
    }
}
