<?php

namespace App\Http\Controllers;
use Response;
use App\Navette;
use App\Patient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;

class NavetteController extends Controller
{

 public function __construct(){

        $this->middleware('auth');

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
    	$navettes=Navette::all();
        return view('navettes.index',compact('navettes'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        return view('navettes.create');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $this->validate($request,[
            'code_hospital'=>'required',
            'mdp_hospital'=>'required',
            'num_patient'=>'required',
           
            'name'=>'required',
            'last_name'=>'required',
           'adress'=>'required', 
           'name_medica'=>'required',  
            ]);
        $navette = new Navette();
        $navette->code_hospital = $request['code_hospital'];
        $navette->mdp_hospital = $request['mdp_hospital'];
        $navette->num_patient = $request['num_patient'];
        $navette->num_patient = $request['name'];
        $navette->last_name = $request['last_name'];
        $navette->adress = $request['adress'];
        $navette->name_medica = $request['name_medica'];

        $navette->save();

        return redirect()->route('navettes.index')
        ->with('flash_message','navette ajoute');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\navette  $navette
     * @return \Illuminate\Http\Response
     */

    public function show(navette $navette)
    {
        return redirect()->route('navettes.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\navette  $navette
     * @return \Illuminate\Http\Response
     */

    public function edit(navette $navette)
    {
        return view('navettes.edit',compact('navette'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\navette  $navette
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, navette $navette)
    {
        $this->validate($request,[
            'name'=>'required',
            'prix'=>'required',
            'code_navette'=>'required'
            ]);
        $navette->name = $request['name'];
        $navette->code_navette = $request['code_navette'];
        $navette->prix = $request['prix'];
        $navette->save();

        return redirect()->route('navettes.index')
        ->with('flash_message','navette mais a jour');
   
    }
   

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\navette  $navette
     * @return \Illuminate\Http\Response
     */

    public function destroy(Request $request,$id){
        

        $navette = Navette::find($id);
        $navette->deleted  = 1;
        dd($navette);
        $navette->save();
       
    


        return redirect()->route('navettes.index')
        ->with('flash_message','navette supprimé');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\navette  $navette
     * @return \Illuminate\Http\Response
     */
    public function delete($id){
        

        $navette = Navette::find($id);
        $navette->deleted  = 1;
        $navette->save();
  
        return redirect()->route('navettes.index')
        ->with('flash_message','navette supprimé');
    }

    function getPatient(){
        $id = Input::get('id');
        $patient = Patient::find($id);
        return response()->json($patient);
    }
}
