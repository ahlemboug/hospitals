<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
     public function scopeSearch($query, $s){
   	return $query->where('last_name','like', $s.'%');
   }

}
