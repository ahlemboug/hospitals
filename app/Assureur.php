<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assureur extends Model
{
    
   public function scopeSearch($query, $s){
   	return $query->where('name','like', $s.'%');
   }
}
